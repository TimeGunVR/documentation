\section{Design and Asset Creation\\ \small{by Konstantin Gomm}}

This section provides an overview on how the game scene was assembled, the process of modeling objects and creating materials, and how the different objects are implemented and rendered in the game. 

\subsection{Modeling \& Materials}
All objects used in Time Gun VR have been modeled using the free 3D modeling software \textit{Blender}. The goal was to keep the polygon count as low as possible while not losing too much detail, to guarantee both, a visually appealing experience, and a high framerate, which is always important in a virtual reality context. Therefore most detail was added using high quality textures and a physically based rendering (PBR) approach. 

After exporting the meshes to \textit{Substance Painter}, a painting software for texturing 3D assets, three to four textures have been created for each object in the game: a base color texture, a normal map, a map for ambient occlusion, roughness, and metallic values packed into one texture's RGB channels, and optionally an emissive map. The textures were then again imported into Blender to setup the objects' materials using a PBR node provided by \textit{Khronos Group} for Blender's cycles renderer in their \textit{Blender glTF 2.0 Exporter} repository.

The blender files for every object were then combined to assemble a scene for each room in one single blender file. Finally, with help of Khronos Group's blender glTF exporter the whole blender file including all scenes, objects and material information was exported as a file in glTF format. 

\begin{figure}[ht]
  \centering
  \includegraphics[width=\linewidth]{pictures/models.png}
  \caption{Some models in the game (from top left to bottom right: Keypad, Safe, Computer, Floppy Disk)}
\end{figure}

\subsection{Object Implementation}
To import the scene file into the game, the \textit{AssetManager} class was created. It is responsible for loading the scene from the glTF file via Magnum's TinyGltf importer plugin, and associating the different objects with their respective textures to setup their PBR materials. Contrary to the objects, which are only loaded in the game according to the current scene, all textures are loaded when the application starts and stored in the asset manager to reduce loading times between scene transitions due to the high resolution textures.

Furthermore the asset manager differentiates between various objects in the loading process via their model name, and instantiates different specialized object types accordingly. There is one base class for all objects in the game:

\begin{description}
\item [Base Object] Every object in the game is derived from this class. It is responsible for loading and storing the object's meshes, rigidbody, material and drawable.
\end{description}
Static objects without further functionality (e.g. walls) are directly instantiated as base objects, however, by creating a separate specialized object class (e.g. Keypad, Safe, etc.) more features and interaction opportunities can be added by additionally deriving from one or multiple of the following classes:

\begin{description}
\item [Time Object] Objects that are derived from this class can be influenced by the player using the time gun. Implementation-wise, this is basically a animation that must be specified in \textit{updateTimeAnimation()} by the derived classes, which is automatically called on the currently targeted time object in every draw event. Also, the animation starting point and duration can be modified by setting \textit{animationTime} and \textit{animationDuration}. To catch when the player starts or stops to shoot the time ray at the object, \textit{onTargeted()} and \textit{onUntargeted()} can be implemented. 

Examples: Computer, Keypad, Safe

\item [Grab Object] Objects that are derived from this class can be picked up or moved by the player using the wand trigger button. Whenever the button is pressed while the controller overlaps with a virtual object, a function call to \textit{grab()} establishes a basic constraint \ref{con} between the player's controller and the grabbed object. The function can additionally be overriden by the specialized object class to implement further functionality. When releasing the trigger button, \textit{ungrab()} is called to let go of the object by removing the constraint.

Examples: Floppy Disk, Safe (dial \& door), Seeds

\item [Interactable Object] Objects that are derived from this class require no button press for interaction. This is realized via a simple triggering mechanic. As soon as the player's controller overlaps with a virtual object, \textit{onTriggerEnter()} is called on that object once. Vice versa \textit{onTriggerExit()} is called when the objects don't overlap anymore. Specialized object classes can override these functions to react to the players interaction.

Examples: Keypad
\end{description}

\subsection{Drawing}
As mentioned before all objects in Time Gun VR with one exception are drawn using a physically based rendering approach. Therefore, for each mesh of a base object a \textit{DrawablePBR} is instantiated, which stores references to the mesh, and its texture and material properties summarized in an instance of \textit{MaterialPBR}. DrawablePBR is derived from Magnum's \textit{Drawable3D} and overrides its \textit{draw()} function to render the mesh using the vertex and fragment shaders loaded in the \textit{PBR\textunderscore Shader} class, which implements Magnum's \textit{AbstractShaderProgram}. The actual shader files \textit{PBR\textunderscore Shader.vert} and \textit{PBR\textunderscore Shader.frag} were adapted from KhronosGroup's \textit{glTF-WebGL-PBR} repository and modified to fit the needs of this project. The most important changes are the use of one combined texture for ambient occlusion, roughness, and metallic values, implementing a point light source, and adding an optional alpha cutoff. 

One exception to the rule is the time ray, which is not rendered via the PBR shader. Since the ray varies in length depending on the player's and the target object's position, it was decided to generate the mesh at runtime. For that reason, an instance of the \textit{TimeRay} class is created when the application starts. Whenever the player shoots a ray into the scene, the time ray objects is added to the drawables, and \textit{recalculateMesh} is called to generate a line of vertices along the ray. The vertex positions are additionally modified using a cubic bezier curve and a helix equation, to make the resulting mesh more interesting. The mesh is then drawn multiple times with different rotations using the \textit{PolyboardShader} class. Polyboards are a billboarding technique where not only a single quad is generated from a vertex, but rather a series of connected triangles from a line of vertices. \cite[p.~258~sqq.]{polyboard}. This is realized by using a geometry shader (\textit{Polyboard.geom}) inbetween the vertex and fragment shader stage, which receives a vertex line as an input, and outputs a triangle strip. Finally, by offsetting the vertices' texture coordinates and also modifying some parameters of the bezier curve and helix equation in each frame, the time ray appears to be in motion and flowing in the player's direction when rewinding, and away from the player when forwarding time.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\linewidth]{pictures/timeray.png}
  \caption{Time ray, generated using a polyboard approach}
\end{figure}